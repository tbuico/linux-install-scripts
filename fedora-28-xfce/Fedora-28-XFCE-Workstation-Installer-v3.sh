${current_user}#!/bin/bash
# sudo chmod u+x Fedora-28-XFCE-Workstation-Installer-v3.sh && sudo sh Fedora-28-XFCE-Workstation-Installer-v3.sh

# User Controlled Variables:
# ----------

## Names:
  script=Fedora-28-installer
  log_file=Fedora-28-install.log
  git_user="hungrykanamit"
  git_email="thomasjbuico@gmail.com"
## Paths:
  log_path=/var/log
  current_user=tom
  user_home=/home/${current_user}
### Packages:
  unwanted_packages="\
    abiword \
    asunder \
    claws-mail \
    geany \
    gnumeric \
    parole \
    pragha \
    xfce4-clipman-plugin \
    ";
  rpms="\
    http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-28.noarch.rpm \
    http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-28.noarch.rpm \
    https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm \
    ";
  groups="\
    'C Development Tools and Libraries' \
    'Development Tools' \
    ";
  strd_packages="\
    curl-devel \
    dnf-automatic \
    faad2-devel \
    ffmpeg-devel \
    git-all \
    gnutls-devel \
    gtk3-devel \
    guake \
    json-c-devel \
    libao-devel \
    libgcrypt-devel \
    libmad-devel \
    libreoffice \
    nmap \
    nodejs \
    ntp \
    p7zip \
    python-gtkextra \
    rrdtool \
    sane-backends \
    sane-backends-drivers-scanners \
    screen \
    simple-scan \
    whois \
    xclip \
    xdotool \
    xfce-theme-manager \
    zsh \
    ";
  repo_packages="\
    fuse-exfat \
    gimp \
    grub-customizer \
    kodi \
    pianobar \
    pithos \
    steam \
    VirtualBox \
    vlc \
    ";
  theme_packages="\
    arc-theme \
    breeze-cursor-theme \
    numix-icon-theme-circle \
    pop-icon-theme \
    ";
    ## Other themes that are nice
    # paper-icon-theme
    # adapta-gtk-theme
    # deepin-icon-theme
    # numix-icon-theme
    # evopop-gtk-theme
    # evopop-icon-theme
    # xenlism-wildfire

# Script Needed Variables:
# ----------

## Error Messages:
  fatal_1="Fatal: This script must be run as root. Do you even sudo \!\!"
  fatal_2="Fatal: This script requires an internet connection."
## Colors:
  ### Reset Color:
    Color_Off='\e[0m'       # Text Reset
  ### Regular Colors:
    Black='\e[0;30m'        # Black
    Red='\e[0;31m'          # Red
    Green='\e[0;32m'        # Green
    Yellow='\e[0;33m'       # Yellow
    Blue='\e[0;34m'         # Blue
    Purple='\e[0;35m'       # Purple
    Cyan='\e[0;36m'         # Cyan
    White='\e[0;37m'        # White

# Functions:
# ----------

chk_root() {
  if [[ ${EUID} -ne 0 ]]
  then
    printf "   ${Red}${fatal_1}${Color_Off}\n"
    exit 1
  fi
}

chk_network() {
  if ! nslookup google.com 2>&1 >/dev/null
  then
    printf "   ${Red}${fatal_2}${Color_Off}\n"
    exit 1
  fi
}


# Script:
# ----------

# touch ${log_path}/${log_file}

## Start logging
{
start=`date +%s`
printf "\n${UPurple}   -----   "${script}" Started @ `date '+%F_%H-%M-%S'`   -----   ${Color_Off}\n"

## Check if running as root, exit if not
chk_root

## Check if online, exit if not
chk_network

## Set kernal limit in dnf.con
printf "   ${Green}Editing dnf.conf${Color_Off}\n"
sed -i "s/installonly_limit=3/installonly_limit=2/" /etc/dnf/dnf.conf

## Remove Unwanted Packages
printf "   ${Green}Removing unwanted packages${Color_Off}\n"
dnf remove -y ${unwanted}
dnf -y autoremove
dnf -y clean all

## Do Initial Upgrade
printf "   ${Green}Running Initial Upgrade${Color_Off}\n"
dnf -y upgrade --refresh

## Install all the RPMs
printf "   ${Green}Installing all the RPMs${Color_Off}\n"
dnf install -y ${rpms}
dnf install -y http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-28.noarch.rpm
dnf install -y http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-28.noarch.rpm
dnf install -y https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm

## Install Adobe Flash
printf "   ${Green}Installing Adobe Flash${Color_Off}\n"
dnf install -y http://linuxdownload.adobe.com/adobe-release/adobe-release-x86_64-1.0-1.noarch.rpm
dnf install -y flash-plugin
# Restart your browser and open following page for testing Adobe Flash Plugin:
# Check if Adobe Flash Plugin Working: https://get.adobe.com/flashplayer/about/
# On Google Chrome you have to enable Flash from Settings -> Advanced -> Content settings -> Flash -> Ask First then you have allow/block sites or manually click every flash items.

## Installing all the groupinstalls
printf "   ${Green}Installing all the groupinstalls${Color_Off}\n"
dnf -y upgrade
dnf -y groupinstall ${groups}
dnf -y groupinstall "C Development Tools and Libraries"
dnf -y groupinstall "Development Tools"

## Install all the things from strd_packages
printf "   ${Green}Installing all standard packages${Color_Off}\n"
dnf -y upgrade
dnf -y install ${strd_packages}

## Enable dnf-automatic updates
printf "   ${Green}Enabling auto updates${Color_Off}\n"
sed -i "s/apply_updates = no/apply_updates = yes/" /etc/dnf/automatic.conf

## Kodi Pre-configuration
printf "   ${Green}Running kodi pre-config${Color_Off}\n"
usermod ${current_user} -a -G video
usermod ${current_user} -a -G audio

## Install all the things from repo_packages
printf "   ${Green}Installing all the repo packages${Color_Off}\n"
dnf -y upgrade
dnf -y install ${repo_packages}

## Install all the things from theme_packages
printf "   ${Green}Installing all the theme packages${Color_Off}\n"
dnf -y upgrade
dnf -y install ${theme_packages}

## Enable Steam in-home-streaming by opening specified ports
printf "   ${Green}opening ports needed for Steam in-home-streaming${Color_Off}\n"
firewall-cmd --permanent --add-port=4380/udp
firewall-cmd --permanent --add-port=27000-27036/udp
firewall-cmd --permanent --add-port=27015-27030/tcp
firewall-cmd --permanent --add-port=27036-27037/tcp
firewall-cmd --reload
systemctl restart firewalld
# System restart is required

## Install rclone
printf "   ${Green}Installing rclone${Color_Off}\n"
cd ${user_home}
mkdir rclone
cd rclone
curl -O https://downloads.rclone.org/rclone-current-linux-amd64.zip
unzip rclone-current-linux-amd64.zip
rm -rf rclone-current-linux-amd64.zip
cd rclone-*-linux-amd64
cp rclone /usr/bin/
chown root:root /usr/bin/rclone
chmod 755 /usr/bin/rclone
mkdir -p /usr/local/share/man/man1
cp rclone.1 /usr/local/share/man/man1/
mandb

## Install Languages
# printf "   ${Green}Installing Go${Color_Off}\n"
# wget https://dl.google.com/go/go1.10.2.linux-amd64.tar.gz
# sudo tar -C /usr/local -xzf ~/Downloads/go1.10.2.linux-amd64.tar.gz
# export PATH=$PATH:/usr/local/go/bin
printf "   ${Green}Installing Scala${Color_Off}\n"
dnf -y install sbt

## Git Configuration (not working as root, need to switch to $current_user in script)
printf "   ${Green}Configuring Git${Color_Off}\n"
# git config --global color.ui true
sudo -u ${current_user} bash -c "git config --global color.ui true"
# git config --global user.name ${git_user}
sudo -u ${current_user} bash -c "git config --global user.name ${git_user}"
# git config --global user.email ${git_email}
sudo -u ${current_user} bash -c "git config --global user.email ${git_email}"
#- RSA key will be generated and loaded to Github later

## Install latest build of atom
printf "   ${Green}Installing the latest build of Atom${Color_Off}\n"
dnf install -y $(curl -sL "https://api.github.com/repos/atom/atom/releases/latest" | grep "https.*atom.x86_64.rpm" | cut -d '"' -f 4)

## Install IntelliJ IDEA
printf "   ${Green}Installing IntelliJ IDEA${Color_Off}\n"
cd /tmp
wget https://download.jetbrains.com/idea/ideaIC-2018.1.4.tar.gz
tar xf ideaIC*.tar.gz -C /opt/ #jetbrains recommends /opt
chmod -R 755 /opt/idea-IC*
cd ${user_home}

# ## Set up zsh
# printf "   ${Green}Swiching to Zsh and installing Oh-My-Zsh${Color_Off}\n"
# # chsh -s $(which zsh)
# sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
# sudo -u ${current_user} bash -c "sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)""


## Cleanup
printf "   ${Green}Running Cleanup${Color_Off}\n"
dnf -y autoremove
dnf -y clean all
rm -rf /tmp/*
dnf -y upgrade --refresh

## List directions for manual followup
printf "   ${Blue}____________________________________________________________________________${Color_Off}\n"
printf "   ${Cyan}To finish setting up git do the following:${Color_Off}\n"
printf "   ${Cyan}Type: ${Yellow}ssh-keygen -t rsa -C ${git_email} -b 4096${Color_Off}\n"
printf "   ${Cyan}Type: ${Yellow}xclip -sel clip < ${user_home}/.ssh/id_rsa.pub${Color_Off}\n"
printf "   ${Cyan}Sign in to Gitlab and Navigate to the 'SSH Keys' tab in your 'Profile Settings'.${Color_Off}\n"
printf "   ${Cyan}Paste your key in the 'Key' section and give it a relevant 'Title'.${Color_Off}\n"
printf "   ${Blue}____________________________________________________________________________${Color_Off}\n"
printf "   ${Cyan}To configure rclone type: ${Yellow}rclone config${Color_Off}\n"
printf "   ${Blue}____________________________________________________________________________${Color_Off}\n"
printf "   ${Cyan}Please run IntelliJ IDEA's first run script manually with the following command:${Color_Off}\n"
printf "   ${Cyan}Type: ${Yellow}bash /opt/idea-IC*/bin/idea.sh${Color_Off}\n"
printf "   ${Blue}____________________________________________________________________________${Color_Off}\n"
printf "   ${Cyan}To install OhMyZsh type: ${Yellow}sh -c \"\$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)\"${Color_Off}\n"
printf "   ${Cyan}To configue OhMyZsh type: ${Yellow}vi ${user_home}/.zshrc${Color_Off}\n"
printf "   ${Blue}____________________________________________________________________________${Color_Off}\n"
printf "   ${Cyan}For Steam In-Home-Streaming to work, a reboot is needed${Color_Off}\n"
printf "   ${Blue}____________________________________________________________________________${Color_Off}\n"
printf "   ${Cyan}After reboot, check if Adobe Flash is working by visiting:${Color_Off}\n"
printf "   ${Yellow}https://get.adobe.com/flashplayer/about/${Color_Off}\n"
printf "   ${Blue}____________________________________________________________________________${Color_Off}\n"

## End logging
end=`date +%s`
runtime=$((end-start))
printf "\n${UPurple}   -----   "${script}" ended @ `date '+%F_%H-%M-%S'` & Finished in ${runtime} seconds   -----   ${Color_Off}\n"

} 2>&1 | tee ${log_path}/${log_file}
